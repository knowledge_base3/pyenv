# pyenv

## Просто нужно запомнить.

### Автоматический установшщик:

    $ curl https://pyenv.run | bash

Настраиваем оболочку

    $ export PYENV_ROOT="$HOME/.pyenv"
    $ export PATH="$PYENV_ROOT/bin:$PATH"
    $ eval "$(pyenv init --path)"

Перезапускаем терминал и проверяем что pyenv работает:

    $ pyenv --version

    # или
    $ pyenv help

Смотрим список доступных питонов:

    $ pyenv install --list

Устанавливаем какой нибудь питон:

    $ pyenv install 3.12

Теперь проверяем список доступных питонов:

    $ pyenv versions

И видим что то такое:

    * system (set by /home/ddd/.pyenv/version)
      3.12.3

Звездочкой выделен активный питон

### Использование

В папке проекта указываем какой питон должен быть:

    $ pyenv local 3.12.3

В папке проекта появился файл ".python-version" в котором будет указана необходимая версия питона

TODO разобраться с виртуальным окружением для проекта



